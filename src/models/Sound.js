
angular
.module('SoundscapeApp')
    .factory('Sound', ["Nodejs", "Utils", "$log", "$interval", "$q", "ticker", function (Nodejs, Utils, $log, $interval, $q, ticker /*Super*/) {
    
    /**
     * Constructor, with class name
     */
    const States = {
        ERROR : "error",
        FETCHED: "fetched",
        UNLOADED: "unloaded",
        LOADING: "loading",
        LOADED: "loaded"
    }
    const path = Nodejs.path;
    const fs = Nodejs.fs;
    var musicMetadata = Nodejs.musicMetadata;

    var $scope = null;
    var state = undefined;
    var fetched = false;
    var ready = false;

    function xval(object, xpath){
        var chain = (xpath||"").split(".");
        var pointer = object;
        chain.forEach(prop => {
            if (pointer != null && pointer.hasOwnProperty(prop)){
                pointer = pointer[prop];
            }else{
                pointer == null;
            }            
        });
        return pointer;
    }

    function Sound(options, $scope) {
        // Public properties, assigned to the instance ('this')
        this.$class = Sound.$class;   
        //$log.debug("new "+this.$class, options, !!$scope);
        this.options = {
            src: null,
            preload: true,
            mute: false,
            vol: 100
        };
        if(angular.isObject(options)){
            angular.extend(this.options, options || {});      
            this.bootstrap();
        }

    }
    Sound.prototype.modelOf= function(Model){
        return (this.$class === Model.$class || false) || ((this.$super||{}).$class === Model.$class || false);
    }
    Sound.prototype.bootstrap = function(options){
        $log.debug("boot", this.options, options);
        angular.extend(this.options, options || {});  
        var parse = (this.options.src || "null/-noname-.null").match(/.*[\\/](.+?)\.(\w{1,5})$/);
        this.extension = this.extension || this.options.extension || ((angular.isDefined(parse) && (parse||[]).length > 2) ? parse[2] : undefined);
        this.label = this.label || this.options.label || ((angular.isDefined(parse) && (parse||[]).length > 1) ? parse[1] : "-noname-");
        this.duration = this.duration || 0;
        this.played = this.played || 0;
        this.category = this.category || this.options.category;
        this.loop = !!this.options.loop;
        this.vol = angular.isNumber(this.options.vol) ? Math.max(0, Math.min(this.options.vol, 100)) : 100;
        this.mute = !!this.options.mute;
        this.interval = null;
        this.player = null;
        this.metadata = this.metadata || {empty:true};
        this.extra = {};
        if (this.options.src) {
            return this.fetchMetadata().then(
                angular.bind(this, function (metadata) {
                    //this.metadata = metadata;
                    //this.duration = this.duration || xval(metadata, "format.duration") || 0;
                    state = States.FETCHED;
                    if (!!$scope) {
                        this.init($scope);
                    }
                    return $q.resolve(this);
                }),
                angular.bind(this, function (err) {
                    state = States.ERROR;
                    return $q.resolve(this);
                })
            )
        }else{
            return $q.resolve(this);
        }
    }

    Sound.prototype.getMetadata =  function(){
        return this.metadata;
    }

    Sound.prototype.meta = function(xpath){
        return xval(this.metadata||{}, xpath);
    }
    
    Sound.prototype.isFetched=function(){
        return fetched;
    }
    Sound.prototype.isReady=function(){
        return fetched;
    }
    Sound.prototype.isLoaded=function(){
        return this.getState() == States.LOADED;
    }   

    function eventCallback(sound, event, arg){
        var ename = event.charAt(0).toUpperCase() + event.substr(1);
        var callback = sound.options['on'+ename] || angular.noop;
        try {
            callback(sound, arg);
        } catch (err) {
            eventCallback(sound, "error", err);
        }
    }

    Sound.prototype.fetchMetadata= function(){  
        $log.debug("fetchMeta", this.metadata);
        if(!this.metadata.empty){
            return $q.resolve(this.metadata);
        }
        var deferred = $q.defer();        
        musicMetadata.parseFile(this.options.src, { native: false })
        .then(angular.bind(this, function (metadata) {
                this.metadata = metadata;
                var cover = this.meta("common.picture");
                if (angular.isDefined(cover) && angular.isArray(cover)) {  
                    cover = cover[0];                  
                    if (!fs.existsSync("data/library/covers")) {
                        fs.mkdirSync("data/library/covers");
                    }
                    var img = Utils.uuidv4() + "." + cover.format;
                    var imgpath = path.join("data/library/covers", img);
                    fs.writeFile(imgpath, cover.data, 'binary', function (err) {
                        if (err) $log.err(this.$class,"fetcchMetadata", err);
                    });
                    this.metadata.common.picture[0].url = encodeURIComponent(img);
                    this.metadata.common.picture[0].data = undefined;                   
                }
                fetched = true;
                eventCallback(this, "fetched");
                deferred.resolve(this.metadata);
            }))
            .catch(angular.bind(this, function (err) {
               $log.error(this.$class, "metadata", err);
               deferred.reject(err);
            }));
        return deferred.promise;
    }

    Sound.prototype.getState= function(){
        if(state == States.ERROR  || this.player == null){
            return state;
        }else{
            return this.player.state();
        }
    }

    Sound.prototype.init = function(scope){
        $log.debug(this.$class, "INIT", this.options.src);
        $scope = scope;
        var player_options = angular.copy(this.options);
        if(angular.isDefined(player_options.src)){
            var ext = path.extname(player_options.src);
            var filename = encodeURIComponent(path.basename(player_options.src, ext));            
            player_options.src = path.join(path.dirname(player_options.src), filename + ext);
        }    
        this.player = new Howl(player_options);

        this.player.on("end", angular.bind(this, function () {
            $log.debug(this.$class, "event on end");
            this.played = 0;
            $scope.$apply();
        }))
        
        this.player.once("load", angular.bind(this, function () {
            $log.debug(this.$class, "event once load");
            this.duration = this.player.duration();
            $scope.$apply();
        }));
        
        this.player.once("play", angular.bind(this, function () {
            $log.debug(this.$class, "event once play");
            this.interval = ticker.periodical(function () {                                  
                    var played = this.player.seek();
                    this.played = played;                
            },this);
        }))

        ready = true;
        eventCallback(this, "ready");

        return this;
    }

    Sound.prototype.seek = function (position) {
        if (this.player && this.player.state() == States.LOADED) {
            if(!!position){
                $log.debug(this.$class, "seek", position);
            }
            return this.player.seek(position);//.apply(s.player, arguments);
        } else {
            return undefined;
        }
    }

    Sound.prototype.toggleLoop = function () {
        this.loop = this.player.loop(!this.player.loop()).loop();
        $log.debug(this.$class, "loop", this.loop);
        return this.loop;
    }

    Sound.prototype.play = function (resume) {
        if (this.player.state() == States.UNLOADED) {
            $log.debug(this.$class, "loading", this);
            this.player.once("load", angular.bind(this, function () {
                $log.debug(this.$class, "loaded");
                this.play(true);
                $scope.$apply();
            }));
            this.player.load();
        } if (this.player.state() == States.LOADED) {
            if (resume === false) {
                $log.debug(this.$class, "pause");
                this.player.pause();
            } else {
                $log.debug(this.$class, "play");
                this.player.play();
            }
        } else {
            $log.debug(this.$class, "waiting for loading...");
            //wait...                    
        }
    }
    Sound.prototype.toggleMute = function () {
        $log.debug(this.$class, "toggleMute");
        this.mute = !this.mute;
        this.player.mute(this.mute);
    }
    Sound.prototype.stop = function () {
        $log.debug(this.$class, "stop");
        return this.player.stop();
    }
    Sound.prototype.pause = function () {
        $log.debug(this.$class, "pause");
        return this.player.pause();
    }
    Sound.prototype.volume = function (vol) {
        $log.debug(this.$class, "volume", vol);
        return this.player ? this.player.volume(vol) : undefined;
    }
    Sound.prototype.playing = function () {
        //$log.debug(this.$class, "playing");
        return this.player ? this.player.playing() : false;
    }
    
    Sound.prototype.applyVol = function () {
        $log.debug(this.$class, "applyVol", this.vol);
        this.volume(this.vol / 100);
    }

    Sound.prototype.deserialize = function (data) {
        angular.extend(this, data);
        return this;
    }

    Sound.prototype.serialize = function () {
        return {
            options: this.options,
            extension: this.extension,
            label: this.label,
            duration: this.duration,
            category: this.category,
            metadata: this.metadata
        };
    }
    
    /**
     * Static property
     * Using copy to prevent modifications to private property
     */
    Sound.build = function (data) {                
        return new Sound().deserialize(angular.copy(data)).bootstrap();
    };

    Sound.States = angular.copy(States);
    Sound.$class = "Sound";
    Sound.$fullclass = Sound.$class;
    

    /**
     * Static method, assigned to class
     * Instance ('this') is not available in static context
     */
    /**
     * Return the constructor function
     */
    return Sound;
}]);