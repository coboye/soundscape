angular
    .module('SoundscapeApp')
    .factory('Theme', ["Sound","$log", "$interval", function (Sound, $log, $interval /*Super*/) {

    $super = Sound;
    
    function Theme(){
        $super.apply(this, arguments);  
        this.options.loop = this.loop = true;      
        this.$super = $super;
        this.$class = Theme.$class;
    }
    
    Theme.$class = "Theme"
    Theme.$fullclass = [$super.$fullclass,Theme.$class].join(".");


    angular.extend(Theme.prototype, $super.prototype);

    Theme.prototype.constructor = Theme;
    
   

    /**
     * Static method, assigned to class
     * Instance ('this') is not available in static context
     */
    Theme.build = function (data) {
        return $super.build(data);
    };
    /**
     * Return the constructor function
     */
    return Theme;
}]);