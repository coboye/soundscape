angular
    .module('SoundscapeApp')
    .factory('Effect', ["Sound","$log", "$interval", function (Sound, $log, $interval /*Super*/) {

    $super = Sound;
    
    function Effect(){
        $super.apply(this, arguments);
        this.$super = $super;
        this.$class = Effect.$class;
    }
    
    Effect.$class = "Effect";
    Effect.$fullclass = [$super.$fullclass, Effect.$class].join(".");


    angular.extend(Effect.prototype, $super.prototype);

    Effect.prototype.constructor = Effect;
    
   

    /**
     * Static method, assigned to class
     * Instance ('this') is not available in static context
     */
    Effect.build = function (data) {
        return $super.build(data);
    };
    /**
     * Return the constructor function
     */
    return Effect;
}]);