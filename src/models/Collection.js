
angular
.module('SoundscapeApp')
    .factory('Playlist', ["$log", "$interval", "$q", "Sound", function ($log, $interval, $q, Sound) {
    
    var $scope;

    function Playlist(options, tracks) {
        // Public properties, assigned to the instance ('this')
        this.$class = Playlist.$class;   
        $log.debug("new "+this.$class, options, !!$scope);
        this.options = {
            loop: false,
            mute: false,
            vol: 100
        };        
        angular.extend(this.options, options || {});
        this.name = this.options.name || "-playlist-";        
        this.tracks = tracks || [];  
        this.index = -1;
        this.next();              
    }

    Playlist.prototype.shuffle= function() {
        for (let i = this.tracks.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [this.tracks[i], this.tracks[j]] = [this.tracks[j], this.tracks[i]];
        }
        return this.track;
    }

    Playlist.prototype.tracks= function(tracks){
        if(angular.isArray(tracks)){
            this.tracks = tracks;
        }
        return this.tracks;
    }
    
    Playlist.prototype.next= function(){
        var next = this.index+1;
        if(next >= this.tracks.length){
            if(this.options.loop === true){
                next = 0;
            }else{
                next = null;
            }
        }
        this.tracks[this.index].stop();
        this.tracks[next].play();
    }
    
    
    /**
     * Static property
     * Using copy to prevent modifications to private property
     */
    Playlist.States = angular.copy(States);
    Playlist.$class = "Playlist";
    Playlist.$fullclass = Sound.$class;
    

    /**
     * Static method, assigned to class
     * Instance ('this') is not available in static context
     */
    Playlist.build = function (data) {
        return new Sound(
            data
            /*data.first_name,
            data.last_name,
            data.role,
            Organisation.build(data.organisation) // another model*/
        );
    };
    /**
     * Return the constructor function
     */
    return Playlist;
}]);