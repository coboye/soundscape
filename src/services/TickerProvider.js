angular.module("SoundscapeApp")
    .provider("ticker", function () {

        var $interval;
        var $log;

        var stack = [];
        var executing = false;
        var interval = 123;

        function uuidv4() {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }

        var Future = function (callback, context, args, iterations) {
            this.id = uuidv4();
            this.callback = callback || angular.noop;
            this.context = context || null;
            this.args = args || [];
            this.iterations = angular.isNumber(iterations) ? Math.min(this.iterations, 1) : null;
            this.times = 0;
            this.execute = function () {
                if (this.iterations == null || this.times < this.iterations) {
                    //$log.debug("Executing future", this);
                    try {
                        this.times++;
                        this.callback.apply(this.context, this.arguments);
                        return true;                        
                    } catch (err) {
                        $log.error("Executing future", this,err);
                        return false;
                    }
                }
                return false;
            }

        }

        function stop(id) {
            for (var i = stack.length - 1; i >= 0; --i) {
                if (stack[i].id == id) {
                    return (stack.splice(i, 1) || []).length > 0;
                }
            }
        }

        function lock() {
            if (executing) {
                return false;
            }
            return executing = true;
        }

        function unlock() {
            executing = false;
        }

        function loop() {
            if (lock()) {
                //$log.debug("loop");
                try {
                    var failed = [];
                    for (var i = stack.length - 1; i >= 0; --i) {
                        var future = stack[i];
                        if (!future.execute()) {
                            $log.warn("Future failed", future);
                            failed.push(i);
                        }
                    }
                    failed.forEach(index => {
                        stack.splice(index, 1);
                    });
                } catch (err) {
                    $log.error("Ticker Loop Error", err);
                } finally {
                    unlock();
                }

            }

        }

        function once(callback, context, args) {
            return ntimes(callback, context, args, 1);
        }

        function periodical(callback, context, args) {
            return ntimes(callback, context, args, null)
        }

        function ntimes(callback, context, args, iterations) {
            if (angular.isFunction(callback)) {
                var future = new Future(callback, context, args, iterations);
                stack.push(future);
                return future.id;
            }
            return null;
        }

        var looper = undefined;

        function initialize() {
            $log.debug("Ticker initializing looper");
            if (angular.isDefined(looper)) {
                $interval.cancel(looper);
                $log.debug("reset ticker loop", interval)
                looer = undefined;
            }
            looper = $interval(angular.bind(this, loop), interval);
        }

        //initialize();

        return {
            setInterval: function (milliseconds) {
                interval = angular.isNumber(milliseconds) ? Math.max(milliseconds, 100) : interval;
                initialize();
            },
            $get: ['$log', '$interval', function ($$log, $$interval) {
                $log = $$log;
                $interval = $$interval;
                initialize();
                return {
                    times: ntimes,
                    once: once,
                    periodical: periodical,
                    stp: stop
                };
            }]
        };
    });