

angular.module("SoundscapeApp")
    .provider("soundbankManager", ["Nodejs", function (Nodejs) {

        // Persistent datastore with manual loading
        var Datastore = Nodejs.nedb
        const fs = Nodejs.fs;
        const path = Nodejs.path;
        
        const codecs = ["mp3", "mpeg", "opus", "ogg", "oga", "wav", "aac", "caf", "m4a", "mp4", "weba", "webm", "dolby", "flac"];
        
     
        var root_dir = {
            themes: [
            //  "F:/Workspace/Soundscape/Music",
               path.join("data", "themes")
            ],
            effects: [
           //     "F:\\Workspace\\Soundscape\\RoyaltyFree\\Effects",
                path.join("data", "effects")
            ]
        }


        var allowed = [];

        var $log;
        var $q;

        function Source(directory, fullpath){
            this.directory=directory;
            this.fullpath = fullpath;
        }

        function initialize() {
            allowed = [];
            codecs.forEach(codec => {
                if(Howler.codecs(codec)){
                    allowed.push(codec);
                }
            });
            $log.debug("Soundbank Manager initializing ", root_dir, allowed);
        }

        function isAllow(file){
            var parse = file.match(/.*?\.(\w+?)$/);
            if (!!parse && parse.length > 1 && allowed.includes(parse[1].toLowerCase())){
                return true;
            }
            return false;
        }

        function scanDir(directory, recursive){
            var sounds = [];
            try {
                fs.readdirSync(directory).forEach(file => {
                    var fpath = path.join(directory, file);
                    var stat = fs.lstatSync(fpath);
                    if(stat.isDirectory()){
                        if(recursive){
                            $log.debug("directory << ", file);
                            sounds = sounds.concat(scanDir(fpath, false));
                        }
                    }else if(stat.isFile()){
                        $log.debug("file >> ", file);
                        if (isAllow(file)) {
                            sounds.push(new Source(directory, fpath));
                        }
                    }
                });
            } catch (error) {
                $log.error(directory, error);
            }
            return sounds;
        }

        function fetch(type){
            var directories = root_dir[type];
            var deferred = $q.defer();
            if (angular.isDefined(directories)){
                var sounds = [];
                directories.forEach(directory => {                      
                    sounds = sounds.concat(scanDir(directory, true));        
                });
                deferred.resolve(sounds);               
            }else{
                $log.error("wrong type", type);
                deferred.reject();
            }
            return deferred.promise;
        }
        
        function root(config){
            if(angular.isDefined(config) && angular.isObject(config)){
                root_dir = config;
                initialize();
            }
            return angular.copy(root);
        }
        
        return {
            setRoot: function (rootConfig) {
                root_dir = rootConfig;
                initialize();
            },
            $get: ['$log', '$q', function ($$log, $$q) {
                $log = $$log;
                $q = $$q;
                initialize();
                return {
                    fetch:fetch,
                    root: root           
                    
                };
            }]
        };
    }])
    .factory("soundbank", ["Nodejs", "soundbankManager", "library", "Theme", 'Effect', '$log', '$q', function (Nodejs, soundbankManager, library, Theme, Effect, $log, $q) {        
        
        const path = Nodejs.path;
             
        const Types={
            THEME : {library:"themes", model: Theme},
            EFFECT: {library:"effects", model: Effect},
        }
        
        function getSounds(Type, fileoptions){
            $log.debug("Soundbanl:getSounds", Type.library);
            var deferred = $q.defer();
            library.findAll(Type.library).then(
                function(docs) {
                    var sounds = [];
                    if(docs && docs.length>0){
                        var promises = [];                       
                        docs.forEach(record => {                            
                            promises.push(
                                (function(){                                   
                                    angular.extend(record.options, fileoptions || {});
                                    return Type.model.build(record)
                                        .then(sound =>{                                           
                                            sounds.push(sound);
                                            return $q.resolve();
                                        });
                                })()
                            )                                
                        });                      
                        $q.all(promises).then( function () {                                                      
                            deferred.resolve(sounds);
                        });
                    }else{
                        soundbankManager.fetch(Type.library).then(
                            function (sources) {
                                var promises = [];
                                var sounds = [];
                                sources.forEach(source => {
                                    promises.push((function(){
                                        var options = { src: source.fullpath, category: path.basename(source.directory) };
                                        angular.extend(options, fileoptions || {});
                                        return new Type.model().bootstrap(options)
                                        .then(sound=>{
                                            return library.insert(Type.library, sound.serialize())
                                                .then(record => {
                                                    sound._id = record._id;
                                                    sounds.push(sound);
                                                    return $q.resolve();
                                                })                                         
                                        })
                                    })())                          
                                });
                                $q.all(promises).then(transport =>{
                                    deferred.resolve(sounds);
                                })
                            },
                            function (error) {
                                deferred.reject(error);
                            }
                        )
                    }                    
                }
            ); 
            return deferred.promise;
        }
        
        return {            
            themes:angular.bind(this, getSounds, Types.THEME),
            effects:angular.bind(this, getSounds, Types.EFFECT)             
        };
    }]);