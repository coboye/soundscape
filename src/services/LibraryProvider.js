angular.module("SoundscapeApp")
    .provider("library", ["Nodejs", function (Nodejs) {

        // Persistent datastore with manual loading
        const Datastore = Nodejs.nedb;
        const path = Nodejs.path;
        const musicMetadata = Nodejs.musicMetadata;    

        var $log, $q, $timeout;

        var ready_promise = false;

        var library = {
            themes: new Datastore({ filename: path.join("data", "library", "themes.json") }),
            effects: new Datastore({ filename: path.join("data", "library", "effects.json") }),
        }

        function all(namespace){            
            return ready().then(ready=>{
                var deferred = $q.defer();
                if (angular.isDefined(namespace) && angular.isDefined(library[namespace])){
                    library[namespace].find({}, function (err, docs) {
                        if(!!err){
                            $log.error("Library:all",arguments, err );
                            deferred.reject(err);
                        }else{
                            $log.info("Library:all",namespace, docs.length);
                            deferred.resolve(docs);
                        }
                    });               
                }
                return deferred.promise;
            })
        }

        function insert(namespace, record) {
            return ready().then(ready => {
                var deferred = $q.defer();
                if (angular.isDefined(namespace) && angular.isDefined(library[namespace])) {
                    if(angular.isArray(record)){
                        for (let index = 0; index < record.length; index++) {
                            delete record[index]._id;                        
                        }
                    }else{
                        delete record._id;
                    }
                    $log.error("Library:inserting...", namespace, record);
                    library[namespace].insert(record, function (err, docs) {
                        if (!!err) {
                            $log.error("Library:insert", namespace, record);
                            deferred.reject(err);
                        } else {
                            $log.info("Library:insert", namespace, docs.length);
                            deferred.resolve(docs);
                        }
                    });
                }
                return deferred.promise;
            });
        }
        function upsert(namespace, record) {
            return ready().then(ready =>{
                var deferred = $q.defer();
                if (angular.isDefined(namespace) && angular.isDefined(library[namespace])) {
                    if(angular.isArray(record)){
                        $log.error("Library:upsert", arguments, "array not allowed");
                        return $q.reject("array not allowed");
                    }
                    if(angular.isDefined(record._id)){
                        library[namespace].find({_id:record._id}, function (err, docs) {
                            if (!!err) {
                                $log.error("Library:upsert", arguments, err);
                                deferred.reject(err);
                            } else {
                                if(docs && docs.length > 0){                               
                                    library[namespace].update({ _id: record._id }, { returnUpdatedDocs: true}, function(err, numdoc, docs){
                                        $log.info("Library:upsert", namespace, numdoc);
                                        deferred.resolve(docs);
                                });
                            }else{
                                return insert(namespace, record);
                            }
                            }
                        });
                    }
                
                }
                return deferred.promise;
            });
        }
       

        function ready(){
            return ready_promise.promise;
        }


        function initialize(store, attemps, def){
            $log.info("initializing", (store||{}).filename);
            var deferred = def || $q.defer();
            store.loadDatabase(function (err) {    // Callback is optional
                // Now commands will be executed
                if(err){
                    $log.warn("cannot intialize", store.filename, attemps||0, err);
                    $timeout(initialize, 1, true, store, (attemps||0)+1, deferred);
                }else{
                    $log.warn("namespace initialized!", store.filename, attemps || 0);
                    deferred.resolve(attemps|| 0);
                }
            })
            return deferred.promise;             
        }

        
        return {
            $get: ['$log', '$q', "$timeout", function ($$log, $$q, $$timeout) {
                $log = $$log;
                $q = $$q;
                $timeout = $$timeout;
                ready_promise = $q.defer();
                initialize(library.themes)
                .then(
                    attemps=>{
                        return initialize(library.effects);
                    })
                .then(ready_promise.resolve)
                
                return {         
                    findAll: all,
                    ready:ready,
                    upsert: upsert,
                    insert: insert                    
                };
            }]
        };
    }]);