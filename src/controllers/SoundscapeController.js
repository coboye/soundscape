angular
.module('SoundscapeApp')
    .controller('SoundscapeController', ['Nodejs', 'Effect', 'Theme', 'Colors', '$scope', '$rootScope', '$q', '$timeout', '$interval', '$log', '$filter', 'ticker', 'soundbank',
        function (Nodejs, Effect, Theme, Colors, $scope, $rootScope, $q, $timeout, $interval, $log, $filter, ticker, soundbank) {
       
        const path = Nodejs.path;
        var ctrl = this;
    
        ctrl.toggle = toggle;
        ctrl.soundSeek = soundSeek;
        ctrl.sounds = [];
        ctrl.progressStyle = progressStyle;
        ctrl.coverStyle = coverStyle;

        function soundSeek($event, sound){
            if(sound.duration>0){
                var el = event.target
                var pos = event.offsetX / el.clientWidth * sound.duration
                $log.debug("seek to ", pos);
                sound.seek(pos);
            }
        }

        function initBank(){
            var options = { preload: false };
            $q.all([
                  soundbank.themes(options),
                  soundbank.effects(options),
        /*        (function () {
                    var deferred = $q.defer();
                    soundbank.effects(options).then(
                        records => {
                            $log.info(">> effects", records.length);
                            var sounds = [];                            
                            records.forEach(sound => {
                                sounds.push(decorator(sound.init($scope)));
                            });
                            return deferred.resolve(sounds);
                        }
                    )
                    return deferred.promise;
                })()*/
            ]).then(function(resolved){
                var sounds = []
                resolved.forEach(set => {
                    sounds = sounds.concat(set);
                });
                function compare(field){
                    return function(a, b) {
                        if (a[field] < b[field])
                            return -1;
                        if (a[field] > b[field])
                            return 1;
                        return 0;
                    }
                }
                sounds.sort(
                    Nodejs
                        .sortBy("$class")
                        .thenBy("category")
                        .thenBy("label", { ignoreCase: true })
                );
                $log.debug(">> sounds", sounds.length);
                sounds.forEach(sound => {
                    ctrl.sounds.push(decorator(sound.init($scope)));
                });
                //this.sounds = sounds;
            });

           
        }    
        
        function toggle(sound) {
            if (sound.playing()) {
                sound.stop();
            } else {
                sound.play();
            }
        }

        function progressStyle(sound){
            var cssw = 0;            
            if(sound.duration > 0){
                cssw = sound.played  / sound.duration * 100;
            }
            return {
                width: $filter("number",2)(cssw)+"%"
            }
        }
        function decorator(sound){
            try {
              sound = coverStyle(sound);  
            } catch (error) {
                $log.warn("decorator", error);
            }
            return sound;
        }
        
        function coverStyle(sound){
            var css = {};
            var cover = sound.meta("common.picture");
            var colors = Colors.rgb(Colors.mood(sound.$class+":"+sound.category));
            sound.extra.coverstyle = {
                "background-color": "rgba("+colors.r+","+colors.g + "," + colors.b+" , 0.15)"
            }
            if(angular.isDefined(cover) && angular.isArray(cover)){                
                cover = cover[0];
                var data;
                if (cover.url){
                    data = ("data/library/covers/" + decodeURIComponent(cover.url));
                }else if(cover.data){
                    var type = cover.format
                    $log.debug("cover", cover.format, typeof cover.data);
                    var prefix = "data:" + type + ";base64,";
                    var base64 = new Buffer(cover.data, 'binary').toString('base64');
                    data = prefix + base64;
                }
                
                css["border"]= "none";
               // css["backgruond-image"]='url("'+data+'")';
                sound.extra.coverstyle ={"background-image" : 'url(' + data + ')'};
            }
            return sound;            
        }

        initBank();
}])