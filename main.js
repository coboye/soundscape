const electron = require('electron')
const { app, BrowserWindow } = electron
const path = require('path')
const url = require('url')
const soundsFolder = './sounds/';
const fs = require('fs');
const { exec } = require('child_process');
const config = require('dotenv').config();


// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win

const ffmpeg = path.resolve(__dirname, "./lib/ffmpeg-win64/bin/ffmpeg.exe");

if (config.error) {
    throw config.error
}

console.log(config.parsed);

function generateWaveforms(){
    fs.readdirSync(path.resolve(__dirname,soundsFolder)).forEach(file => {
        console.log(file);
        if(file.endsWith(".mp3")){
            exec('"ffmpeg" -y -i "'
            + path.resolve(__dirname, soundsFolder, file) 
            + '" -filter_complex "compand,showwavespic=s=1000x300" -frames:v 1 "'+ path.resolve(__dirname, soundsFolder,"wave-"+ file)+'.png"', (err, stdout, stderr) => {
                if (err) {
                    console.error(err);
                    return;
                }
                console.log(stdout);
            });
        }
    })
    //.\ffmpeg - i..\..\..\sounds\growl.mp3 - filter_complex "showwavespic=s=640x120" - frames: v 1..\..\..\sounds\growl.png
}
//generateWaveforms();

function createWindow() {
    const { width, height } = electron.screen.getPrimaryDisplay().workAreaSize
    // Create the browser window.
    win = new BrowserWindow({
        width: width,
        height: height, 
        simpleFullscreen:true
    })

    // and load the index.html of the app.
    win.loadURL(url.format({
        pathname: path.join(__dirname, '/src/windows/main.html'),
        protocol: 'file:',
        slashes: true
    }))

    // Open the DevTools.
    win.webContents.openDevTools()

    // Emitted when the window is closed.
    win.on('closed', () => {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        win = null
    })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
        createWindow()
    }
})